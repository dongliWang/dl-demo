package com.beike.dldemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DlDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DlDemoApplication.class, args);
    }

}
